FROM sewatech/apache

# Install https, yarn and node 7.
RUN apt-get update && apt-get -y install apt-transport-https \
  && apt-get -y install curl \
  && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
  && apt-get update && apt-get install yarn

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get -y install nodejs


RUN mkdir -p /var/www/ibm
WORKDIR /var/www/ibm
COPY . /var/www/ibm
COPY ./config/sampleapp.conf /etc/apache2/sites-available/sampleapp.conf

RUN a2ensite sampleapp.conf

RUN yarn install
RUN npm run webpack-prod

EXPOSE 80
