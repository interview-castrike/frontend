# IBM Sample App UI for Managing Tasks

## Getting Started
This UI was developed for IBM's Interview Sample Application.
It has been built on React with React Router.
The CSS is compiled using SASS and served independently from the applications javascript.


### Prerequisites
This UI needs a webserver of your choice.  In order to compile its assets, node v7.10.1 and yarn are needed.
Moreover, it uses http://api.ibmsampleapp.com:3000 as the api url. `api.ibmsampleapp.com` needs to point to the right IP..

### Installing
run ```yarn install```

### Compiling the assets
* For development run ```npm run webpack-dev```
* For production run ```npm run webpack-prod```


#### To Dos
* Component Testing using jest.
* Add redux for the applications state management.
