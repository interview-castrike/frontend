import React from 'react';
import ReactDOM from 'react-dom';

import { HashRouter as Router, Link, Route } from 'react-router-dom';

import Tasks from './components/Tasks';
import Task from './components/Task';
import TaskForm from './components/TaskForm';

class IBMSampleApp extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <header>
            <h1>Task Manager</h1>
          </header>
          <nav>
            <ul>
              <li><Link to="/">List of Tasks</Link></li>
              <li><Link to="/add">Add Task</Link></li>
            </ul>
          </nav>
          <main>
            <Route exact path="/" component={Tasks} />
            <Route exact path="/add" component={TaskForm} />
            <Route exact path="/task/:id" component={Task} />
            <Route exact path="/update/:id" component={TaskForm} />
          </main>
          <footer>
            IBM Sample App by Cristobal Sepulveda
          </footer>
        </div>
      </Router>
    );
  }
}

ReactDOM.render( <IBMSampleApp/>, document.getElementById('ibm-sample-app') );
