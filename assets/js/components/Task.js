import React from 'react';
import { Link } from 'react-router-dom';

class Task extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      task: {}
    }
  }
  componentDidMount() {
    fetch('http://api.ibmsampleapp.com:3000/tasks/'+this.props.match.params.id)
      .then( (rawResponse) => {
        if( rawResponse.status === 200) {
          return rawResponse.json();
        } else {
          throw( rawResponse );
        }
      })
      .then( response => {
        this.setState({task: response.data});
      })
      .catch( err => {
        console.error( err );
      });
  }

  render() {
    return (
      <div className="task">
        <h2 className="task-title">{this.state.task.name}</h2>
        <p className="task-description">{this.state.task.description}</p>
        <div className="task-date task-dueDate">{this.state.task.dueDate}</div>
        <div className="task-date task-completedDate">{this.state.task.completedDate || '-'}</div>
        <Link to={'/update/'+this.state.task.id}>Edit Task</Link>
      </div>
    );
  }
}

export default Task;
