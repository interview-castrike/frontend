import React from 'react';
import queryString from 'query-string';
import { Link } from 'react-router-dom';

const baseTask =  {
  name: '',
  description: '',
  dueDate: '',
  completedDate: ''
};

class TaskForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      message: {
        type: '',
        text: ''
      },
      task: Object.assign({}, baseTask)
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.deleteTask   = this.deleteTask.bind(this);
  }

  componentDidMount() {
    if( this.props.match.params.id ) {
      fetch('http://api.ibmsampleapp.com:3000/tasks/'+this.props.match.params.id)
        .then( (rawResponse) => {
          if( rawResponse.status === 200) {
            return rawResponse.json();
          } else {
            throw( rawResponse );
          }
        })
        .then( response => {
          let editTask = Object.assign({}, baseTask, response.data);
          this.setState({task: editTask});
        })
        .catch( err => {
          console.error( err );
        });
    } else {
      this.setState({task: Object.assign({}, baseTask)});
    }
  }

  deleteTask(event) {
    event.preventDefault();
    if( !this.state.task.id ) return;
    fetch('http://api.ibmsampleapp.com:3000/tasks/delete',{
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: queryString.stringify({ id: this.state.task.id })
    }).then( (rawResponse) => {
      if( rawResponse.status === 200) {
        return rawResponse.json();
      } else {
        throw( rawResponse );
      }
    }).then( ( response )=> {
      let { message } = response;
      message += ' You will be redirected shortly to the main dashboard shortly.';
      this.setState({message: {type:'success', text: message}, task: Object.assign({}, baseTask) });
      setTimeout(()=> {
        this.props.history.push('/');
      }, 2500);
    }).catch( (err) => {
      this.setState({message: {type:'error', text: 'Task could not be deleted. Please try again'}});
      console.error( err );
    });
  }

  handleChange(event) {
    var newState = Object.assign({}, this.state);
    newState.task[event.target.id] = event.target.value;
    this.setState(newState);
  }

  handleSubmit(event) {
    event.preventDefault();
    const saveUrl = ( this.state.task.id ) ? 'http://api.ibmsampleapp.com:3000/tasks/update/'+this.state.task.id : 'http://api.ibmsampleapp.com:3000/tasks/add';
    fetch(saveUrl,{
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: queryString.stringify(this.state.task)
    }).then( (rawResponse) => {
      if( rawResponse.status === 200) {
        return rawResponse.json();
      } else {
        throw( rawResponse );
      }
    }).then( ( response )=> {
      let { message, data } = response;
      if( !this.state.task.id ) {
        message += ' You will be redirected shortly to the main dashboard shortly.';
        setTimeout(()=> {
          this.props.history.push('/');
        }, 2500);
      }
      this.setState({message: {type:'success', text: message}, task: data });

    }).catch( (err) => {
      this.setState({message: {type:'error', text: 'Task could not be added. Please try again'}});
      console.error( err );
    });
  }

  render() {
    return (
      <form className="form" onSubmit={this.handleSubmit}>
        <div className={'form-message form-message-'+this.state.message.type}>{this.state.message.text}</div>
        <div className="form-item">
          <label htmlFor="name" className="form-item-label">Name:</label>
          <input id="name" type="text" className="form-item-input" value={this.state.task.name} onChange={this.handleChange} />
        </div>
        <div className="form-item">
          <label htmlFor="description" className="form-item-label">Description:</label>
          <textarea id="description" className="form-item-textarea" onChange={this.handleChange} value={this.state.task.description}></textarea>
        </div>
        <div className="form-item-dates">
          <div className="form-item">
            <label htmlFor="dueDate" className="form-item-label">Due Date:</label>
            <input id="dueDate" type="date" className="form-item-input" value={this.state.task.dueDate} onChange={this.handleChange} placeholder="mm/dd/yyyy" />
          </div>
          <div className="form-item">
            <label htmlFor="completedDate" className="form-item-label">Completed Date:</label>
            <input id="completedDate" type="date" className="form-item-input" value={this.state.task.completedDate} onChange={this.handleChange} placeholder="mm/dd/yyyy" />
          </div>
        </div>
        <div className="form-submit">
          <input  className="form-submit-button" type="submit" value={ (this.state.task.id) ? 'save task' : 'add task' } />
          <button className={ (this.state.task.id) ? 'form-submit-button showDelete': 'hideDelete' } onClick={this.deleteTask}>Delete</button>
        </div>
      </form>
    );
  }
}

export default TaskForm;
