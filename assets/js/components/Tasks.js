import React from 'react';
import { Link } from 'react-router-dom';

class Tasks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      filter: 'all'
    }

    this.setFilter = this.setFilter.bind(this);
  }
  componentDidMount() {
      fetch('http://api.ibmsampleapp.com:3000/tasks')
        .then( (rawResponse) => {
          if( rawResponse.status === 200) {
            return rawResponse.json();
          } else {
            throw( rawResponse );
          }
        })
        .then( (response) => {
          // Response
          const responseAsArray = Object.keys( response.data ).map( taskIndex => response.data[taskIndex] );
          this.setState({tasks: responseAsArray});
        })
        .catch( (err) => {
          console.error( err );
        });
  }

  setFilter(event){
    this.setState({filter:event.target.dataset.filter});
  }

  applyFilter() {
    let today = new Date();
    today.setHours(0, 0, 0, 0);
    switch(this.state.filter) {
      case 'today':
        return this.state.tasks.filter( (task) => {
          return task.dueDate == today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        });
        break;
      case 'tomorrow':
        return this.state.tasks.filter( (task) => {
          return task.dueDate == today.getFullYear()+'-'+(today.getMonth()+1)+'-'+(today.getDate()+1);
        });
        break;
      case 'overdue':
        return this.state.tasks.filter( (task) => {
          const dueDate = new Date(task.dueDate.replace( /\-/g, '\/'));
          return ( dueDate.getTime() < today.getTime() ) && !task.completedDate;
        });
        break;
      case 'completed':
        return this.state.tasks.filter( (task) => {
          return task.completedDate.length > 0;
        });
        break;
      default:
        return this.state.tasks;
    }
  }

  render() {
    if( !this.state.tasks.length ) {
      return (
        <div className="tasks">
          No tasks fit this criteria. Please use a different filter or add a task
        </div>
      );
    } else {
      const tasks = this.applyFilter();
      return (
        <div>
          <ul className="filters">
            <li className="filters-filter"><button className="filters-filter-button" data-filter="all" onClick={this.setFilter}>All</button></li>
            <li className="filters-filter"><button className="filters-filter-button" data-filter="today" onClick={this.setFilter}>Due Today</button></li>
            <li className="filters-filter"><button className="filters-filter-button" data-filter="tomorrow" onClick={this.setFilter}>Due Tomorrow</button></li>
            <li className="filters-filter"><button className="filters-filter-button" data-filter="overdue" onClick={this.setFilter}>Over Due</button></li>
            <li className="filters-filter"><button className="filters-filter-button" data-filter="completed" onClick={this.setFilter}>Completed</button></li>
          </ul>
          <table className="tasks">
            <thead>
              <tr><td>Name</td><td>Due Date</td><td>Completed</td><td></td></tr>
            </thead>
            <tbody>
            {tasks.map( (task,i) => {
              return (
                <tr key={i}><td>{task.name}</td><td>{task.dueDate}</td><td>{task.completedDate.length > 0 ? 'Y' : 'N'}</td><td className="links"><Link to={'/task/'+task.id}>View Task</Link> <Link to={'/update/'+task.id}>Edit Task</Link></td></tr>
              );
            })}
            </tbody>
          </table>
        </div>
      );
    }

  }
}

export default Tasks;
