const webpack = require('webpack');
const path = require('path');

const PUBLIC_DIR = path.resolve(__dirname, 'public/');
const ASSETS_DIR = path.resolve(__dirname, 'assets/');


const ExtractTextPlugin = require("extract-text-webpack-plugin");

const config = {
  entry: [ASSETS_DIR + '/js/app.js', ASSETS_DIR+'/css/app.scss'],
  output: {
    path: PUBLIC_DIR,
    filename: 'bundle.js'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      include: ASSETS_DIR+'/js',
      loader: 'babel-loader'
    },{
      test: /\.(sass|scss)$/,
      include: ASSETS_DIR+'/css',
      loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
    }]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'bundle.css',
      allChunks: true,
    }),
  ]
};

module.exports = config;
